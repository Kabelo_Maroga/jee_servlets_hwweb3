package com.psybergate.jeefnds.servlets.arch3.client.controller;

import java.sql.SQLException;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;

import com.psybergate.jeefnds.servlets.hwweb3.customer.Customer;
import com.psybergate.jeefnds.servlets.hwweb3.database.DatabaseManager;

public class CustomerController {

	public String addCustomer(HttpServletRequest req) throws ClassNotFoundException {

		String status = " add unsuccessful... ";

		try {
			DatabaseManager databaseManager = new DatabaseManager();
			Customer customer = new Customer(req.getParameter("customerNum"), req.getParameter("name"),
					req.getParameter("surname"), formatDate(req.getParameter("dateOfBirth")));
			databaseManager.persist(customer);
			status = " add successful...";
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	private LocalDate formatDate(String dateOfBirth) {
		return LocalDate.parse(dateOfBirth);
	}
}