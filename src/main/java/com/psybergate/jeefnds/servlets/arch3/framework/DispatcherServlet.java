package com.psybergate.jeefnds.servlets.arch3.framework;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.psybergate.jeefnds.servlets.arch3.framework.controllermapping.ControllerMethod;

/**
 * This version of Dispatcher shows: <br>
 * 1. Single servlet handling all requests <br>
 * 2. Controller architecture with multiple requests per controller<br>
 */
@WebServlet(urlPatterns = { "/customer/*" }, loadOnStartup = 1)
public class DispatcherServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final String VERSION = "1.02-DispatcherServlet3:";

	public static final Map<String, ControllerMethod> REQUEST_MAPPING = new HashMap<>();

	@Override
	public void init() throws ServletException {
		loadProperties();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/views/customer.jsp");
		requestDispatcher.forward(req, resp);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getMessage("Thread : " + Thread.currentThread().getName());
		getMessage("Instance : " + this.getClass() + " - " + System.identityHashCode(this));
		RequestDispatcher dispatcher = null;
		try {
			String pathInfo = request.getPathInfo().substring(1); // strip off leading "/"
			System.out.println(getMessage("pathinfo : " + pathInfo));
			ControllerMethod controllerMethod = REQUEST_MAPPING.get(pathInfo);
			if (controllerMethod == null) {
				throw new RuntimeException("No Controller for request with pathinfo : " + pathInfo);
			}
			Object controller = controllerMethod.getController();
			Method method = controllerMethod.getMethod();
			String controllerResponse = (String) method.invoke(controller, request);

			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println(getMessage(controllerResponse));
			out.println("</html>");
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public void loadProperties() {
		getMessage("In Init...");
		getMessage("Loading url-mappings...");
		Map<String, Object> controllers = new HashMap<>();

		try {
			InputStream is = getServletContext().getResourceAsStream("/WEB-INF/url_mappings.properties");

			Properties props = new Properties();
			props.load(is);
			for (Enumeration<?> e = props.propertyNames(); e.hasMoreElements();) {
				String request = (String) e.nextElement();
				String value = props.getProperty(request);
				StringTokenizer tokenizer = new StringTokenizer(value, "#");
				String className = tokenizer.nextToken();
				String methodName = tokenizer.nextToken();
				Class clazz = Class.forName(className);
				Object controller = controllers.get(className);
				if (controller == null) {
					controller = clazz.newInstance();
					controllers.put(className, controller);
				}
				Method method = clazz.getMethod(methodName, HttpServletRequest.class);

				REQUEST_MAPPING.put(request, new ControllerMethod(controller, method));
			}
		} catch (Exception ex) {
			throw new RuntimeException("Error - loading url_mappings file.", ex);
		}

		getMessage("Url Mappings are as follows:");
		for (Map.Entry<String, ControllerMethod> entry : REQUEST_MAPPING.entrySet()) {
			System.out.println("url: " + entry.getKey() + " controller:"
					+ entry.getValue().getController().getClass().getSimpleName());
			System.out.println("url: " + entry.getKey() + " method :" + entry.getValue().getMethod().getName());
		}
	}

	private static String getMessage(String string) {
		return VERSION + string;
	}
}
