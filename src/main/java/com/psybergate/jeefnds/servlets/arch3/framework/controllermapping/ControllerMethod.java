package com.psybergate.jeefnds.servlets.arch3.framework.controllermapping;

import java.lang.reflect.Method;

public class ControllerMethod {

  private Object controller;

  private Method method;

  public ControllerMethod(Object controller, Method method) {
    super();
    this.controller = controller;
    this.method = method;
  }

  public Object getController() {
    return controller;
  }

  public Method getMethod() {
    return method;
  }
}
