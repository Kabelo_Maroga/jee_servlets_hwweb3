package com.psybergate.jeefnds.servlets.hwweb3.customer;

import java.time.LocalDate;

public class Customer {

	private String customerNum;

	private String name;

	private String surname;

	private LocalDate dateOfBirth;

	public Customer(String customerNum, String name, String surname, LocalDate dateOfBirth) {
		this.customerNum = customerNum;
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
	}

	public String getCustomerNum() {
		return customerNum;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
}