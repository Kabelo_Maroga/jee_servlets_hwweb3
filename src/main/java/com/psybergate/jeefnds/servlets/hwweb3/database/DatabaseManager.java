package com.psybergate.jeefnds.servlets.hwweb3.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.psybergate.jeefnds.servlets.hwweb3.customer.Customer;

public class DatabaseManager {

	private Connection connection;

	private Statement statement;

	public DatabaseManager(String url, String user, String password) throws SQLException {

		loadDrivers();
		connection = DriverManager.getConnection(url, user, password);
		statement = connection.createStatement();
	}

	public DatabaseManager() throws SQLException, ClassNotFoundException {
		this("jdbc:postgresql://localhost:5432/kabelo_01", "postgres", "Masalane@01");
	}

	private void loadDrivers() {
		try {
			Class.forName("org.postgresql.Driver");
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void persist(Customer customer) throws SQLException {

		String values = String.format("'%1s', '%2s', '%3s', '%4s'", customer.getCustomerNum(), customer.getName(),
				customer.getSurname(), customer.getDateOfBirth());

		statement.executeUpdate("INSERT INTO customers values(" + values + ")");
	}
}